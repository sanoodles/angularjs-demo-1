'use strict';

// Define the `ajsdemo1App` module
angular.module('ajsdemo1App', [
  'ngAnimate',
  'ngRoute',
  'focus-if',
  'core',
  'game',
  'newGame',
  'highscoreList'
]);
