'use strict';

angular.
  module('ajsdemo1App').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/highscore-list', {
          templateUrl: 'src/highscore-list/highscore-list.template.html'
        }). 
        when('/new-game', {
          templateUrl: 'src/new-game/new-game.template.html'
        }).
        when('/game', {
          templateUrl: 'src/game/game.template.html'
        }).
        otherwise('/highscore-list');
    }
  ]);
