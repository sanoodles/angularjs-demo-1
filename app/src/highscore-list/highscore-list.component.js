'use strict';

angular.
  module('highscoreList', []).
    controller('HighscoreListController', ['$scope', '$location', 'Score',
      function HighscoreListController($scope, $location, Score) {
        $scope.highscores = [];
        Score.query();
        $scope.caption = 'Highscore list';

        $scope.$on('highscoresReceived', function(event, highscores) {
          $scope.highscores = $scope.highscores.concat(highscores);
          $scope.$apply();
        });

        $scope.newGame = function() {
          $location.path('/new-game');
        }

      }
    ]);

