'use strict';

angular.
  module('game', []).
    constant('CONSTANTS', {
      'KEYCODES': {
        'BACKSPACE': 8,
        'DELETE': 46,
      },
      'COLORS': {
        'SUCCESS': '#8f8',
        'FAILURE': '#f88',
      },
      'NOTIFICATION_PERIOD_IN_MS': 150
    }).
    controller('GameController', ['CONSTANTS', '$scope', '$timeout', '$window', '$location', 'CurrentGame', 'Score',
      function GameController(CONSTANTS, $scope, $timeout, $window, $location, CurrentGame, Score) {
        $scope.state = CurrentGame.state;
        $scope.wordBg = 'transparent';
        $scope.placeholder = CurrentGame.getSolution();

        $scope.$on('timeIsUp', function() {
          Score.save({ name: $scope.state.name, score: $scope.state.score });
        });

        $scope.$on('scoreSaved', function(event, error) {
          $scope.$apply(function() {
            if (error) {
              $window.alert('Time is up. Your score was ' + $scope.state.score + '.\n'
                  + '\n'
                  + 'Backend score storage is disabled as this is an only-frontend demo with public access.\n'
                  + '\n'
                  + 'Thank you for your understanding.');
              $location.path('/highscore-list');
            } else {
              $window.alert('Time is up. Your score was ' + $scope.state.score + '.');
              $location.path('/highscore-list');
            }
          });
        });

        $scope.skipWord = function() {
          CurrentGame.nextWord();
          angular.element('#entered-word').focus();
        };

        $scope.quitGame = function() {
          if ($window.confirm('Do you really want to quit the game?')) {
            CurrentGame.end();
            $location.path('/highscore-list');
          } else {
            $timeout(function() {
              angular.element('#entered-word').focus();
            }, 50);
          }
        };

        $scope.onKeyDown = function($event) {
          if ($event.keyCode == CONSTANTS.KEYCODES.BACKSPACE ||
              $event.keyCode == CONSTANTS.KEYCODES.DELETE) {
            if ($scope.state.enteredWord.length > 0) {
              $scope.state.currentReward--;
              if ($scope.state.currentReward < 1) {
                CurrentGame.nextWord();
                $scope.wordBg = CONSTANTS.COLORS.FAILURE;
                $timeout(function() {
                  $scope.wordBg = 'transparent';
                }, CONSTANTS.NOTIFICATION_PERIOD_IN_MS);
              }
            }
          }
        };

        $scope.onChange = function() {
          if ($scope.state.enteredWord.toLowerCase() == CurrentGame.getSolution()) {
            $scope.state.score += $scope.state.currentReward;
            CurrentGame.nextWord();
            $scope.wordBg = CONSTANTS.COLORS.SUCCESS;
            $scope.placeholder = '';
            $timeout(function() {
              $scope.wordBg = 'transparent';
            }, CONSTANTS.NOTIFICATION_PERIOD_IN_MS);
          }
        };

      }
    ]);

