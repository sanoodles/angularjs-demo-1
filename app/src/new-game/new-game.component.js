'use strict';

angular.
  module('newGame', []).
    constant('KEYCODE', {
      'ENTER': 13
    }).
    controller('NewGameController', ['KEYCODE', '$scope', '$window', '$location', 'CurrentGame',
      function NewGameController(KEYCODE, $scope, $window, $location, CurrentGame) {
        $scope.state = CurrentGame.state;

        $scope.isResetDone = false;
        $scope.$on('resetDone', function() {
          $scope.isResetDone = true;
        });
        CurrentGame.reset();

        $scope.startGame = function() {
          $location.path('/game');
          CurrentGame.start();
        };

        $scope.onKeyPress = function($event) {
          if ($event.keyCode == KEYCODE.ENTER &&
              $scope.isResetDone &&
              $scope.state.name.length > 0) {
            $scope.startGame();
          }
        };

      }
    ]);

