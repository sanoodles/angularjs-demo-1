'use strict';

const GAME_DURATION_IN_SECONDS = 40;

angular.
  module('core.currentGame').
  factory('CurrentGame', ['$resource', '$interval', '$rootScope', 'SortedWords',
    function($resource, $interval, $rootScope, SortedWords) {

      var timer;

      var state = {
        name: '',
        sortedWords: [],
        sortedWordsIterator: 0,
        unsortedWord: '',
        enteredWord: '',
        currentReward: 0,
        score: 0,
        timeLeft: 0, // in seconds
      };

      var shuffleArray = function(a) {
        var j, x, i, n = a.length;
        for (i = n; i; i--) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
      }

      var getUnsortedWord = function(word) {
          var a = word.split('');
          while (a.join('') == word) {
            shuffleArray(a);
          }
          return a.join('');
      }

      var getSolution = function() {
          return state.sortedWords[state.sortedWordsIterator];
      };

      var getRewardForWord = function(word) {
        return Math.floor(Math.pow(1.95, word.length / 3));
      };

      $rootScope.$on('sortedWordsReceived', function(event, sortedWords) {
        shuffleArray(sortedWords);
        state.sortedWords = sortedWords;
        state.sortedWordsIterator = 0;
        state.unsortedWord = getUnsortedWord(getSolution());
        state.enteredWord = '';
        state.currentReward = getRewardForWord(getSolution());
        state.score = 0;
        state.timeLeft = GAME_DURATION_IN_SECONDS;
        $rootScope.$broadcast('resetDone');
      });

      var currentGame = {

        state: state,

        reset: function() {
          state.name = '';
          SortedWords.query();
        },

        start: function() {
          timer = $interval(function() {
            state.timeLeft--;

            if (state.timeLeft < 1) {
              $interval.cancel(timer);
              $rootScope.$broadcast('timeIsUp');
            }
          }, 1000);

        },

        nextWord: function() {
          state.sortedWordsIterator++;
          state.unsortedWord = getUnsortedWord(getSolution());
          state.enteredWord = '';
          state.currentReward = getRewardForWord(getSolution());
        },

        getSolution: getSolution,

        end: function() {
          $interval.cancel(timer);
        },

      };

      return currentGame;
    }
  ]);

