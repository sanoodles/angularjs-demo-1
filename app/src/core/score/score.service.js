'use strict';

angular.
  module('core.score').
  factory('Score', ['$rootScope',
    function($rootScope) {
      return {
        query: function() {
          var highscoresRef = firebase.database().
          ref('scores/highscores').
          once('value').
          then(function(snapshot) {
            $rootScope.$broadcast('highscoresReceived', Object.values(snapshot.val()));
          });
        },
        save: function(score) {
          firebase.database().ref('scores/highscores').push(score, function(error) {
            $rootScope.$broadcast('scoreSaved', error);
          });
        }
      };
    }
 ]);

