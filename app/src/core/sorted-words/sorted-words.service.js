'use strict';

angular.
  module('core.sortedWords').
  factory('SortedWords', ['$rootScope',
    function($rootScope) {
      return {
        query: function() {
          var sortedWordsRef = firebase.database().
          ref('sorted-words/words-for-a-game').
          once('value').
          then(function(snapshot) {
            $rootScope.$broadcast('sortedWordsReceived', Object.values(snapshot.val()));
          });
        },
      };
    }
 ]);

