module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        //grunt task configuration will go here
        concat: {
            js: { //target
                src: [
                  "./app/src/bower_components/jquery/dist/jquery.js",
                  "./app/src/bower_components/angular/angular.js",
                  "./app/src/bower_components/angular-animate/angular-animate.js",
                  "./app/src/bower_components/angular-resource/angular-resource.js",
                  "./app/src/bower_components/angular-route/angular-route.js",
                  "./app/src/bower_components/ng-focus-if/focusIf.min.js",
                  "./app/src/app.module.js",
                  "./app/src/app.config.js",
                  "./app/src/core/**/*.js",
                  "./app/src/highscore-list/highscore-list.module.js",
                  "./app/src/highscore-list/highscore-list.component.js",
                  "./app/src/new-game/new-game.module.js",
                  "./app/src/new-game/new-game.component.js",
                  "./app/src/game/game.module.js",
                  "./app/src/game/game.component.js",
                ],
                dest: './app/dist/bundle.js'
            }
        },
        uglify: {
            js: { //target
                src: ['./app/dist/bundle.js'],
                dest: './app/dist/bundle.js'
            }
        }
    });

    //load grunt tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    //register grunt default task
    grunt.registerTask('default', ['concat', 'uglify']);
}

